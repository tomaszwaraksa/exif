# EXIF Script
This script is an attempt to turn the mighty ExifTool available at https://exiftool.org/ into something easier for a casual user like me ;-)

Exiftool is probably the most comprehensive and awesome EXIF utility ever. Yet its variety of options and switches is just too tedious for simple cases like mine. Rather than expecting you to memorize all these options and switches, the script asks a couple of questions about the camera, lens, film etc. and set the appropriate EXIF tags.

The script has been tested on MacOS and Linux operating system. It won't work out of the box on Windows, unless you run it on Windows 10 under WSL - Windows Subsystem for Linux.

Click here to see the tool in action:

https://asciinema.org/a/Kqtu35AlPf2yaAXDj7dwhVjUb

# Installation
The script requires `exiftool` to be present on your system. Download and install the `exiftool` following these official instructions: https://exiftool.org/install.html

Then, download `exif` script from BitBucket home at https://bitbucket.org/tomaszwaraksa/exif/. Best is to place it somewhere in the system path, so that it can be executed from any folder without much hassle. A good place for it could be `/usr/local/bin` folder.

Make sure the script can be executed, by running for example

    chmod +x /usr/local/bin/exif

Run the script with command

    exif

The script will ask you for things such as artist name, camera name etc. The entered values will be stored and used as defaults each time you use the script. Of course, you will be then able to enter different settings - this is just a handy help to save you from typing, if you use the same camera and lens frequently.

The default values are be stored in `~/.exifrc` file. You can edit this file manually to change the defaults or run

    exif --configure

# Usage
Run the script from a folder in which your photos to tag with EXIF data are stored:

    exif

The script will display the current path and ask you to enter:

* Artist - most probably you ;-)
* Camera - brand and model of the camera, for example `Contax 139 Quartz`
* Lens - brand and model of the lens, for example `Yashica ML 1.9`
* Focal Length - focal length of the lens, in millimeters, for example `50`
* Film Brand - film brand, for example `Ilford Delta`
* ISO Speed - film sensitivity (ISO), for example `100`

Confirm the entries with `ENTER` and the script will store the provided values in all images found in the working folder. Otherwise press `C` or `CTRL+C` to stop the script.

# Credits
The script has been authored by me, while `ExifTool` is a sole creation and intellectual property of Phil Harvey, https://exiftool.org/.


Enjoy!


Tomasz Waraksa<br>
https://www.flickr.com/tatakazika<br>
Dublin, 2020
